# **tiffbaker** #
Scriptbased automator app which averages the first 30 frames from a movie file into a high resolution tiff. Idea is to use night time drone 4k movies which have been shot as static as possible. tiffbaker will make use of ffmpeg, enfuse, align_image_stack(hugin), imagemagick(convert), dcraw, exiftool. No programs included. Dependency check will take place once the app is started.

## HOWTO ##

Drag a movie file on tiffbaker.app. Preferrably static drone footage. Once processing finished an averaged tiff file will be produced.
Possible also to create darkframe PNG by double tapping tiffbaker.app.

**regarding gatekeeper**

To supress gatekeeper hold ctrl or cmd button down(macOS Sierrra, Mojave, Catalina) while right clicking/opening the application the first time. You can also change permissions from within privacy/security settings.

**Dependencies:**ffmpeg,dcraw,exiftool,imagemagick,enfuse, align_image_stack(hugin) 
#Copyright Danne