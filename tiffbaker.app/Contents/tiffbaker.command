#hugin dependency
if ! [ -f "/Applications/Hugin/Hugin.app/Contents/MacOS/align_image_stack" ]
then
printf '\e[8;16;85t'
printf '\e[3;410;100t'
clear
echo $(tput bold)"
Checking for hugin, please wait..."
sleep 2
read -p $(tput bold)"hugin is not installed would you like to install it?$(tput setaf 1)
(Y/N)?$(tput sgr0)
" choice
case "$choice" in 
  y|Y ) 
#!/bin/bash
clear
echo "Follow instructions in terminal window"
sleep 2
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install --cask --no-quarantine hugin
if [ -f "/Applications/Hugin/Hugin.app/Contents/MacOS/align_image_stack" ]
then
clear && echo "hugin is installed and ready for use"
else
clear && echo "hugin did not install"
fi
sleep 2
;;
  n|N ) 
clear
echo "no thanks!"
sleep 1
;;
  * ) 
echo "invalid selection, let´s start again"
sleep 1
;;
esac
fi

#check for dcraw
if ! [ -f "/usr/local/bin/dcraw" ]
then
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install dcraw
fi


#dependency checker
#exiftool
if ! [ -f "/usr/local/bin/exiftool" ]
then
printf '\e[8;16;85t'
printf '\e[3;410;100t'
clear
echo $(tput bold)"
Checking for exiftool, please wait..."
sleep 2
read -p $(tput bold)"exiftool is not installed would you like to install it?$(tput setaf 1)
(Y/N)?$(tput sgr0)
" choice
case "$choice" in 
  y|Y ) 
#!/bin/bash
clear
echo "Follow instructions in terminal window"
sleep 2
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install exiftool
if [ -f "/usr/local/bin/exiftool" ]
then
clear && echo "exiftool is intalled and ready for use"
else
clear && echo "exiftool did not install"
fi
sleep 2
;;
  n|N ) 
clear
echo "no thanks!"
sleep 1
;;
  * ) 
echo "invalid selection, let´s start again"
sleep 1
;;
esac
fi


#ffmpeg
if ! [ -f "/usr/local/bin/ffmpeg" ]
then
printf '\e[8;16;85t'
printf '\e[3;410;100t'
clear
echo $(tput bold)"
Checking for ffmpeg, please wait..."
sleep 2
read -p $(tput bold)"ffmpeg is not installed would you like to install it?$(tput setaf 1)
(Y/N)?$(tput sgr0)
" choice
case "$choice" in 
  y|Y ) 
#!/bin/bash
clear
echo "Follow instructions in terminal window"
sleep 2
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install ffmpeg
if [ -f "/usr/local/bin/ffmpeg" ]
then
clear && echo "ffmpeg is installed and ready for use"
else
clear && echo "ffmpeg did not install"
fi
sleep 2
;;
  n|N ) 
clear
echo "no thanks!"
sleep 1
;;
  * ) 
echo "invalid selection, let´s start again"
sleep 1
;;
esac
fi


#check for image_magick
if ! [ -f "/usr/local/bin/convert" ]
then
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install image_magick
fi


#enter file folder
cd "$(cat /tmp/movielist | head -1 | perl -p -e 's/'"$(grep -o '[^/]*$' //tmp/movielist | head -1)"'//g')"

if grep 'MOV\|mov\|mp4\|MP4\|mkv\|MKV\|avi\|AVI' /tmp/movielist; then
clear
read -p $(tput bold)"Do you want to run auto white balance?$(tput setaf 1)
(Y/N)?$(tput sgr0)
" choice
case "$choice" in 
  y|Y ) 
awb=$(echo yes)
clear
echo "Applying auto white balancing..."
sleep 1
;;
  * ) 
clear
echo "Skipping auto white balancing..."
sleep 1
;;
esac
fi

while grep 'MOV\|mov\|mp4\|MP4\|mkv\|MKV\|avi\|AVI' /tmp/movielist; do

mkdir -p tmptiff
rm /tmp/reference.tiff 

#if you want auto white balance applied
if [ $awb = yes ];then

#create the haldc file
ffmpeg -f lavfi -i haldclutsrc=8 -compression_algo raw -pix_fmt rgb24 -vframes 1 /tmp/clut.tif
#create the reference tif
ffmpeg -i "$(cat /tmp/movielist | head -1)" -compression_algo raw -pix_fmt rgb24 -vframes 1 /tmp/reference.tiff

"$(cat /tmp/tbcontents)"autocolor /tmp/reference.tiff /tmp/reference.tif

#add needed dng tag. Will open up in dcraw but not in acr. Doesn´t matter, good old dcraw is all we need
exiftool -DNGVersion=1.4.0.0 -PhotometricInterpretation='Linear Raw' /tmp/clut.tif /tmp/reference.tif -overwrite_original

#now you have dcraw compatible dng/raw files
mv /tmp/clut.tif /tmp/clut.dng 
mv /tmp/reference.tif /tmp/reference.dng

#add auto white balance
multiplier=$(dcraw -T -a -v /tmp/reference.dng 2>&1 | awk '/multipliers/ { print $2,$3,$4,$5; exit }')

#apply white balance to your haldc lut
dcraw -T -4 -H 2 -r $multiplier /tmp/clut.dng

#mission accomplished, let´s remove the intermediate dng files
rm /tmp/clut.dng /tmp/reference.dng

  if [ -f darkframe.PNG ];then
    ffmpeg -i "$(cat /tmp/movielist | head -1)" -i /tmp/clut.tiff -filter_complex '[0][1] haldclut' -c:v rawvideo -frames:v 30 -f nut pipe:1 | ffmpeg -i pipe:0 -i darkframe.PNG -filter_complex "format=rgba[a];[a][1]blend=subtract" tmptiff/%05d.tiff
  else
    ffmpeg -i "$(cat /tmp/movielist | head -1)" -i /tmp/clut.tiff -filter_complex '[0][1] haldclut' -pix_fmt rgba -frames:v 30 tmptiff/%05d.tiff
  fi

else
  if [ -f darkframe.PNG ];then
    ffmpeg -i "$(cat /tmp/movielist | head -1)" -i darkframe.PNG -filter_complex "format=rgba[a];[a][1]blend=subtract" -frames:v 30 tmptiff/%05d.tiff
  else
    ffmpeg -i "$(cat /tmp/movielist | head -1)" -pix_fmt rgba -frames:v 30 tmptiff/%05d.tiff
  fi
fi


/Applications/Hugin/Hugin.app/Contents/MacOS/align_image_stack -a tmptiff/aligned.tif tmptiff/*.tiff
/Applications/Hugin/tools_mac/enfuse --exposure-optimum=0.5 --exposure-width=0.5 tmptiff/*.tif -o "$(cat /tmp/movielist | head -1 | cut -d "." -f1)".tiff

rm -r tmptiff

echo "$(tail -n +2 /tmp/movielist)" > /tmp/movielist

done


#if working dng files

while grep 'DNG\|dng' /tmp/movielist; do

mkdir -p tmpHDR
mv $(cat /tmp/movielist) tmpHDR
cd tmpHDR
dcraw -T -H 2 *.DNG
mv *.DNG ../
/Applications/Hugin/Hugin.app/Contents/MacOS/align_image_stack -a aligned.tif *.tiff
/Applications/Hugin/tools_mac/enfuse --exposure-optimum=0.5 --exposure-width=0.5 aligned*.tif -o "$(cat /tmp/movielist | head -1 | cut -d "." -f1)".tiff
rm *.tif *.tiff

rm /tmp/movielist
done

#if working tiff files

while grep 'tiff\|TIFF' /tmp/movielist; do

mkdir -p tmpHDR
/Applications/Hugin/Hugin.app/Contents/MacOS/align_image_stack -a tmpHDR/aligned.tif $(cat /tmp/movielist)
/Applications/Hugin/tools_mac/enfuse --exposure-optimum=0.5 --exposure-width=0.5 tmpHDR/aligned*.tif -o "$(cat /tmp/movielist | head -1 | cut -d "." -f1)".tif
rm tmpHDR/*.tif

rm /tmp/movielist
done


rm /tmp/movielist

